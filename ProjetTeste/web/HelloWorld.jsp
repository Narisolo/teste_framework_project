<%-- 
    Document   : HelloWorld
    Created on : 14 nov. 2022, 08:33:12
    Author     : asus
--%>

<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>hello world page</title>
    </head>
    <body>
         <%
        ServletContext context = request.getServletContext();
        HashMap<String,Object> map = (HashMap<String,Object>) context.getAttribute("data");
        String url = (String) context.getAttribute("url");
        out.print(map.get(url));
    %>
    </body>
</html>
