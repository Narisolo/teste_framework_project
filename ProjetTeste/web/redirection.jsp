<%-- 
    Document   : redirection
    Created on : 11 nov. 2022, 00:27:42
    Author     : asus
--%>

<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>redirection</title>
    </head>
    <%
        ServletContext context = request.getServletContext();
        HashMap<String,Object> map = (HashMap<String,Object>) context.getAttribute("data");
        out.print(map.get("goodbye"));
    %>
    <body>
        <h1></h1>
    </body>
</html>
