<%-- 
    Document   : peronne
    Created on : 16 nov. 2022, 15:31:21
    Author     : asus
--%>

<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Personne</title>
    </head>
    <body>
       <%
        ServletContext context = request.getServletContext();
        HashMap<String,Object> map = (HashMap<String,Object>) context.getAttribute("data");
        out.print((String) map.get("nom"));
        out.print("</br>");
        out.print((String) map.get("prenom"));
        out.print("</br>");
        out.print(map.get("age"));
        out.print("</br>");
        out.print(map.get("date_naissance").toString());
    %>
    </body>
</html>
