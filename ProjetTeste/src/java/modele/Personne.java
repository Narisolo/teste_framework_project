/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import inter.MethodAnnotation;
import java.time.LocalDate;
import java.util.HashMap;
import methodview.ModelView;

/**
 *
 * @author asus
 */
public class Personne {
    private String nom;
    private String prenom;
    private Integer age;
    LocalDate dateNaissance;
    public Personne() {
    }

    public Personne(String nom, String prenom, Integer age,LocalDate date) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.dateNaissance=date;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }
    
    
    public int getAge() {
        return age;
    }
    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }
     @MethodAnnotation(url="personne")
    public ModelView listePersonne(Personne p){
        ModelView mv = new ModelView();
        HashMap<String,Object> map = new HashMap<String,Object>();
        map.put("nom",p.getNom());
        map.put("age",p.getAge());
        map.put("prenom", p.getPrenom());
        map.put("date_naissance",p.getDateNaissance());
        mv.setUrl("/personne.jsp");
        mv.setHmap(map);
      return mv;
    }
    
}
