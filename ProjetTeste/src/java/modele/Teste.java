/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import inter.MethodAnnotation;
import java.util.HashMap;
import methodview.ModelView;

/**
 *
 * @author asus
 */

public class Teste {
    HashMap<String,Object> map;
    
    @MethodAnnotation(url="hello")
    public ModelView HelloWorld(Teste t){
      ModelView mv = new ModelView();
      mv.setUrl("HelloWorld.jsp");
      map = new HashMap<String,Object>();
      map.put("hello","Hello world man !!");
      mv.setHmap(map);
      return mv;
    }
    @MethodAnnotation(url="goodbye")
    public ModelView goodbye(Teste t){
      ModelView mv = new ModelView();
      mv.setUrl("/redirection.jsp");
      map = new HashMap<String,Object>();
      map.put("goodbye","goodbye everybody");
      mv.setHmap(map);
      return mv;
    }
}
